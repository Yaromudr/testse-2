package testSe.abstractEssence;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * ������� �������� ��� ���� ������
 * �������� ������� ������� � ������, ��������� ����������� �������, ���� ����� ��������� ���������� �� ��������
 * Created by A.Dolinskiy on 06.08.2015.
 */
public abstract class Page {
    protected WebDriver driver;
    protected WebDriverWait wait;

    public Page(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 6);

    }
}
