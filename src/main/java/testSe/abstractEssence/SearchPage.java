package testSe.abstractEssence;


import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import testSe.search.SearchNameConstants;
import testSe.target.TargetStartPage;
import testSe.utils.UtilsInfo;

import java.util.Set;


/**
 * ������� �����, ��������� �������� ��������� �������
 * ��������� ������� ����� ������� ������ � ������� �� ������ ������ � ����������� �������
 * Created by A.Dolinskiy on 06.08.2015.
 */
public abstract class SearchPage extends Page {

    private static final Logger LOG = Logger.getLogger(SearchPage.class);

    private By searchLine;
    private By firstLink;
    private WebElement element;

    private String hostname;
    private String findElementName;
    private String locator;

    public SearchPage(WebDriver driver, SearchNameConstants snc) {
        super(driver);
        this.hostname = snc.getHostName();

        this.findElementName = snc.getTag();
        searchLine = By.id(findElementName);

        locator = snc.getFirstLinkLocator();
        firstLink = By.cssSelector(locator);

        driver.get(hostname);
    }

    /**
     * ����� ��� ����������� ������, ������� ��������� ������ � ��������� ������
     *
     * @param text - ����� ���������� �������
     */
    public void find(String text) {
        try {
            assert (!text.isEmpty()) : "��������� ������ �����������";
            LOG.info("���������� ����� �������. ��������: " + text);
            wait.until(ExpectedConditions.visibilityOfElementLocated(searchLine));
            element = driver.findElement(searchLine);
            assert (element != null) : "��� �������� �� ��������";
            LOG.info("������� ��������� ������ �� �������� ������");
            element.sendKeys(text);
            LOG.info("������������ ������� �������");
            element.submit();
            LOG.info("������ ��������� �� ����������");
        } catch (Throwable e) {
            UtilsInfo.warnLog(LOG, e);
        }

    }

    /**
     * ����� �������� �� ������� ��������,
     * ��� ������ ��������� �������� ������
     */
    public TargetStartPage clickFirstLink() {
        try {
            assert (!locator.isEmpty()) : "�������� ��� ������ ������ �� ����������";
            LOG.info("������� ��� ������ ������ ���������");
            wait.until(ExpectedConditions.visibilityOfElementLocated(firstLink));
            element = driver.findElement(firstLink);
            assert (element != null) : "������-��������� �� ����������";
            LOG.info("������-��������� �������");
            element.click();
            LOG.info("����������� ������� �� ������");

            //����� ����������� ��������� �������� ����� �������� �� ��������
            // 2 ������ ��� ����������� ����� ������ �������� � ��� ���� �� ���������
            wait.until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver driver) {
                    LOG.info("���� ��������");
                    Set<String> set = driver.getWindowHandles();
                    return (set.size() == 2);
                }
            });


        } catch (Throwable e) {
            UtilsInfo.warnLog(LOG, e);
        }
        return new TargetStartPage(driver);
    }
}
