package testSe.search;

import org.openqa.selenium.WebDriver;
import testSe.abstractEssence.SearchPage;

/**
 * Created by A.Dolinskiy on 06.08.2015.
 */
public class GooglePage extends SearchPage {
    public GooglePage(WebDriver driver) {
        super(driver, SearchNameConstants.SEARCH_GO);
    }

}
