package testSe.search;

/**
 * Created by A.Dolinskiy on 06.08.2015.
 * ������ �������� ��������� ������.
 * ��������: ����� �����, ��� �������� ��������� ������, �������� ��� ��� �������, ��� ������ ��� ������ ������ ��������� ������
 */
public enum SearchNameConstants {

    SEARCH_YA("http://www.yandex.ru", "text", "yandex", "a[accesskey='1']"),
    SEARCH_GO("http://www.google.com", "lst-ib", "google",  "a[target='_blank']"),
    SEARCH_RA("http://www.rambler.ru", "search_query", "rambler", "a[data-cerber-event='serp::title_1']" );

    private final String hostName;
    private final String tag;
    private final String shortName;
    private       String firstLinkLocator;

    SearchNameConstants(String hostName, String tag, String shortName, String firstLinkLocator) {
        this.hostName = hostName;
        this.tag = tag;
        this.shortName = shortName;
        this.firstLinkLocator = firstLinkLocator;
    }

    public String getTag() {
        return tag;
    }

    public String getHostName() {
        return hostName;
    }

    public String getShortName() {
        return shortName;
    }

    public String getFirstLinkLocator() {
        return firstLinkLocator;
    }
}
