package testSe.start;

import org.apache.log4j.Logger;
import testSe.utils.UtilsInfo;

import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.security.CodeSource;
import java.util.Properties;

/**
 * ����� ��������� ��������� �� �����
 * ������� �������������: � ����������� ���������� ��� �����, ��� ����� � ������, ������������ ���������� jar-�����
 * �.� ���� a.prop ����� ����� � jar �� ���������� ������ "a.prop", ����� - "/path/to/props/a.prop"
 * Created by A.Dolinskiy on 14.08.2015.
 */
class LoaderSettings{
    private static final Logger LOG = Logger.getLogger(LoaderSettings.class);

    private InputStream inputStream;
    private Properties properties;

    private String propFileName;

    /**
     * ��� �������� ����� ������� ���������� ������ ����� �������� � �������� ������� properties � Main
     * ���� �������� ������ ������ ����� ������������ jar-�����, ���� �� ������������ jar-���� ��� ��������� ��������
     */
    public LoaderSettings(String propFileName) {
        this.propFileName = propFileName;
        loadSettings();
    }

    private void loadSettings() {
        LOG.info("������ �������� ���������� �� �����");
        try {
            //�������� ����� ��� �� ���������
            CodeSource src = getClass().getProtectionDomain().getCodeSource();
            URL url;
            if (src != null) {
                //�������� ��� � ����������
                url = new URL(src.getLocation(), propFileName);
                assert (!url.getFile().isEmpty()):"���� � ����������� �� ������";
                LOG.info("������� ���� � �����������");
                inputStream = new FileInputStream(url.getFile());
            }


            properties =new Properties();
            properties.load(inputStream);


        } catch (Throwable e) {
            System.out.println("��������� ���� ��������");
            UtilsInfo.warnLog(LOG, e);
        }
        LOG.info("�������� �������� ���������� �� �����");
    }

    public Properties getProperties() {
        return properties;
    }
}
