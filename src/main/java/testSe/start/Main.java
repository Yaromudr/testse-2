package testSe.start;


import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import testSe.abstractEssence.SearchPage;
import testSe.search.SearchNameConstants;
import testSe.target.TargetFinishPage;
import testSe.target.TargetStartPage;
import testSe.utils.SearchFactory;
import testSe.utils.SearchNameConstantsFactory;
import testSe.utils.UtilsInfo;
import testSe.utils.WebDriverFactory;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Set;


/**
 * ��������� �����
 * Created by A.Dolinskiy on 05.08.2015.
 */
public class Main {

    private static final Logger LOG = Logger.getLogger(Main.class);

    private static Properties prop = new LoaderSettings("resources\\project.properties").getProperties();

    private static String QUERY;
    private static String PATH;
    private static String BROUSERNAME;
    private static String MASK;
    private static String DATEFORMAT;
    private static int numArgs = 2; //����� ���������� �� ����� ���������


    public static Process WebDriwerRemotePROCESS = null;

    private static SearchNameConstants SNC;
    private static SearchPage page = null;
    private static WebDriver driver;

    /**
     * ����� ����������� �������� �� ������ ����� �� ����� ��������
     */
    private static void settings(){

        try {
            QUERY = Main.prop.getProperty("QUERY");
            assert (!(Main.getQUERY()).isEmpty()):"����� ������� �� �������";
            LOG.info("������� ����� ������� �� �����");

            PATH = Main.prop.getProperty("PATH");
            assert (!Main.getPATH().isEmpty()):"����� ���������� �� �������";
            LOG.info("������� ����� ���������� �� �����");

            MASK=  Main.prop.getProperty("MASK");
            assert (!Main.getMASK().isEmpty()):"����� ����� �� ��������";
            LOG.info("�������� ����� �� �����");

            DATEFORMAT = Main.prop.getProperty("DATEFORMAT");
            assert (!Main.getDATEFORMAT().isEmpty()):"������ ���� �� �������";
            LOG.info("������� ������ ���� �� �����");

        } catch (Throwable e) {
            System.out.println("��������� ���� ��������");
            UtilsInfo.warnLog(LOG, e);
        }

    }

    /**
     * ����� ���������� ��������� ��������� � ������ ��������� ������ ������������
     * @param args ������� �� 2 ������: ������ - ������������� ��������, ������ - ��������� �������
     */
    public static void main(String[] args) {

        StringBuilder parametrs = new StringBuilder();
        parametrs.append("\"");
        for (String s : args) {
            parametrs.append(s).append(" ");
        }
        if (args.length != 0) {
            parametrs.deleteCharAt(parametrs.length() - 1);
        }
        parametrs.append("\"");

        LOG.info("�������� �������� �� �����");

        Main.settings();//��������� ������ �� ����� � �����������

        LOG.info("������ ������ � ����������� " + parametrs);
        //��������� ���������



        if ((args.length < numArgs) || (args.length > numArgs)) {
            LOG.info("������� ��������� �� ���������. ���������� ������.");
            UtilsInfo.printInfo();
            System.exit(0);
        }
        //������� ���������
        WebDriver driver_buf = WebDriverFactory.getDriver(args[0]);
        BROUSERNAME = args[0];


        try {
            assert (driver_buf != null) : "�������� ������� �������� ��� ����������� ��������";
            LOG.info("������� ������� ���������");
            driver = driver_buf;
            //������������� �� ���
            driver.manage().window().maximize();
        } catch (Error e) {
            UtilsInfo.warnLog(LOG, e);
        }
        //������� ��������� ������
        SNC = SearchNameConstantsFactory.getConstants(args[1]);

        goToPFLB();


        if (WebDriwerRemotePROCESS != null) WebDriwerRemotePROCESS.destroy();
        LOG.info("������ ��������� ��������");
    }

    /**
     * �������� ����� ������������:
     * 1. ����������� � ������� �������� ��������� �������
     * 2. �������� ������ � ��������� �������
     * 3. ������������� �� ����� ����
     * 4. ���������� ������� �� ���� ������� ��������
     * 5. ������ ����� � ���������� ��� �� ������ � ����
     */
    private static void goToPFLB() {

        try {
            page = SearchFactory.getPage(SNC, driver);
            assert (page != null) : "��� ��������� ��������";
            LOG.info("��������� �������� ��������");
        } catch (Throwable e) {
            UtilsInfo.warnLog(LOG, e);
        }

        String parrWinHandle = driver.getWindowHandle();

        //���������� �����
        page.find(QUERY); //�������� �������� � ����������� �������
        TargetStartPage tp = page.clickFirstLink();//������ ������ ������

        Set<String> sst = driver.getWindowHandles();
        sst.remove(parrWinHandle);

        //������������� ����� ������, ��� ��������� �����
        try {
            assert (sst.size() == 1) : ("�������� ������ Handlers. ������ ���� 1. ��������: " + sst.size());
            LOG.info("������������ ���������: " + parrWinHandle);
            String child = sst.iterator().next();
            assert (!child.isEmpty()) : "��������� ��������� �� ����������";
            LOG.info("�������� ���������: " + child);
            driver.switchTo().window(child);
            LOG.info("������������ ����� ������(����������) ��������� �������");
        } catch (Throwable e) {
            UtilsInfo.warnLog(LOG, e);
        }

        //��������� �� �������� �� ������� ������

        TargetFinishPage finishPage = tp.goToMenu();
        //����� ����
        recordToFile(finishPage.textGruber());
        driver.quit();
        LOG.info("������ ��������� ��������� ���������");
    }

    /**
     * ���������� ������ ����������� ������ � ����
     * ��� ����� ����������� �� ����� ��������, ��������� ����� ��������� �������, ������ ������� � ����� ����������
     * ���������� ��� �������������� � ������ (PATH) ����������� �����, � ������� ����� ��������� ����������.
     * ����� �������� �� �������� � ���� "as is", � ���� ������ ��� �� ���������� ��������� "������" ���������� �����
     * @param text - ������������ �����
     */
    private static void recordToFile(String text) {
        Date now = new Date();
        DateFormat formatter = new SimpleDateFormat(DATEFORMAT);
        String dateStamp = formatter.format(now);

        File file = new File(PATH + BROUSERNAME + "_" + Main.SNC.getShortName() + "_" + dateStamp + "." + MASK);

        try (BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(file))) {
            assert (file.canWrite()) : "���� �� ����� ���� �������";
            LOG.info("�������� ���������� �� ������ �����");
            out.write(text.getBytes("UTF-8"));//������ ��� �������� ������, ��� �������� ������ ���������
            out.flush();
        } catch (Throwable e) {
            UtilsInfo.warnLog(LOG, e);
        }
        LOG.info("������ ������ �������");
    }

    public static WebDriver getDriver() {
        return driver;
    }

    public static Properties getProp() {
        return prop;
    }


    public static String getQUERY() {
        return QUERY;
    }


    public static void setProp(Properties prop) {
        Main.prop = prop;
    }

    public static String getPATH() {
        return PATH;
    }

    public static String getMASK() {
        return MASK;
    }

    public static String getDATEFORMAT() {
        return DATEFORMAT;
    }
}



