package testSe.target;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import testSe.abstractEssence.Page;
import testSe.start.Main;
import testSe.utils.UtilsInfo;

/**
 * Created by A.Dolinskiy on 18.08.2015..
 * �������� ������������� ������������. ����� ���� ����� ������ ������� �����
 */
public class TargetFinishPage extends Page {
    private static final Logger LOG = Logger.getLogger(TargetFinishPage.class);

    private String locatorForText = "user-content";
    private By textArea = By.className(locatorForText);
    private WebElement element;
    private String textFlag;

    public TargetFinishPage(WebDriver driver) {
        super(driver);
        this.textFlag = Main.getProp().getProperty("textFlag");
    }

    /**
     * ����� ������� ����� �� �������� �� ���������� ��������
     * @return ����� �� ��������
     */
    public String textGruber() {
        //�������� ���� ���� ������, � ������ ������ �� ���������
        String text = "";

        try {
            LOG.info("�������� �������� ��������");
            //�������� ��� �����, ��� �� �� ����� �������� ����� �����
            wait.until(ExpectedConditions.visibilityOfElementLocated(textArea));
            element = driver.findElement(textArea);
            assert (element!=null):"������� � ������� �� ������";
            LOG.info("������� ������� ��������� ����");
            LOG.info("��������� ������ �� ��������");
            text = element.getText();
            assert (!text.isEmpty()):"����� �� �������� �������� �� �������";
            LOG.info("����� �� �������� �������");
            int index = text.indexOf(textFlag);
            assert (index >= 0) : "��������� ����� �� ����������";
            text = text.substring(0, index);
            LOG.info("����� ��� ������ �������");

        } catch (Throwable e) {
            UtilsInfo.warnLog(LOG, e);
        }

        return text;

    }
}
