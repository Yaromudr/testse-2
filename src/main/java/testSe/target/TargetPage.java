package testSe.target;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import testSe.abstractEssence.Page;
import testSe.utils.UtilsInfo;

/**
 * Created by A.Dolinskiy on 06.08.2015.
 */
public class TargetPage extends Page {

    private static final Logger LOG = Logger.getLogger(TargetPage.class);

    private String selectorMenu = ".//*[text()='������']/..";//������
    private String selectorMenu2 = ".//*[text()='������������']/..";//������������
    private String selectorMenu3 = ".//*[@id='submenu-item-2']/li[2]/a";//������������� ������������ (���������� �� ������ �������� �� �������)



    private String locatorForText = "user-content";

    public TargetPage(WebDriver driver) {
        super(driver);
    }

    public void goToMenu() {

        try {
            LOG.info("����� ������� �� ����");
            WebElement element = driver.findElement(By.xpath(selectorMenu));
            assert (element!=null):"������� ������� �������� �� ������";
            LOG.info("������ ������ ������� ����");
            Actions actions = new Actions(driver);
            actions.moveToElement(element).build().perform();
            //������� �������
            LOG.info("�������� ������� �������� ����");
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(selectorMenu2)));
            element = driver.findElement(By.xpath(selectorMenu2));
            assert (element!=null):"������� ������� �������� �� ������";
            LOG.info("������ ������ ������� ����");
            actions = new Actions(driver);
            actions.moveToElement(element).build().perform();
            //������� �������
            LOG.info("�������� �������� �������� ����");
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(selectorMenu3)));
            element = driver.findElement(By.xpath(selectorMenu3));
            assert (element!=null):"������� �������� �������� �� ������";
            LOG.info("������ ������ ������� ����");
            element.click();
            LOG.info("����������� ����� �������� �������� ����");
        } catch (Throwable e) {
            UtilsInfo.warnLog(LOG, e);
        }
    }

    public String textGruber() {
        WebElement element = null;
        String text = "";

        try {
            LOG.info("�������� �������� ��������");
            //�������� ��� �����, ��� �� �� ����� �������� ����� �����
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(locatorForText)));
            element = driver.findElement(By.className(locatorForText));
            assert (element!=null):"������� � ������� �� ������";
            LOG.info("������� ������� ��������� ����");
            LOG.info("��������� ������ �� ��������");
            text = element.getText();
            assert (text.isEmpty()):"����� �� �������� �������� �� �������";
            LOG.info("����� �� �������� �������");

        } catch (Throwable e) {
            UtilsInfo.warnLog(LOG, e);
        }

        return text;

    }
}