package testSe.target;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import testSe.abstractEssence.Page;
import testSe.utils.UtilsInfo;

/**
 * �������� �� ������� ���������� ������� �� ����
 * Created by A.Dolinskiy on 06.08.2015.
 */
public class TargetStartPage extends Page {

    private static final Logger LOG = Logger.getLogger(TargetStartPage.class);

    private String selectorMenu = ".//*[text()='������']/..";//������
    private String selectorMenu2 = ".//*[text()='������������']/..";//������������
    private String selectorMenu3 = ".//*[@id=\"submenu-item-2\"]/li[2]/a";//������������� ������������ (���������� �� ������ �������� �� �������)

    By menuStart = By.xpath(selectorMenu);
    By menuSecond = By.xpath(selectorMenu2);
    By menuFinish = By.xpath(selectorMenu3);


    public TargetStartPage(WebDriver driver) {
        super(driver);

    }

    /**
     * ����� �������� �� ���� �� ������� ��������.
     * ��������������� ���������� ����� ���� ��������� ���� � ��������� ����������� ���� �� ����� ����, ��� ��������� ������� ���������� ��������
     */
    public TargetFinishPage goToMenu() {

        try {
            LOG.info("������� ��������� ����"); //���� ��������� � �� ������� ���� �������� �� ����������
            wait.until(ExpectedConditions.visibilityOfElementLocated(menuStart));
            LOG.info("����� ������� �� ����");
            WebElement element = driver.findElement(menuStart);
            assert (element!=null):"������� ������� �������� �� ������";
            LOG.info("������ ������ ������� ����");
            Actions actions = new Actions(driver);
            actions.moveToElement(element).perform();
            //������� �������
            LOG.info("�������� ������� �������� ����");
            wait.until(ExpectedConditions.visibilityOfElementLocated(menuSecond));
            element = driver.findElement(menuSecond);
            assert (element!=null):"������� ������� �������� �� ������";
            LOG.info("������ ������ ������� ����");
            actions.moveToElement(element).perform();
            //������� �������
            LOG.info("�������� �������� �������� ����");
            wait.until(ExpectedConditions.visibilityOfElementLocated(menuFinish));
            element = driver.findElement(menuFinish);
            assert (element!=null):"������� �������� �������� �� ������";
            LOG.info("������ ������ ������� ����");
            element.click();
            LOG.info("����������� ����� �������� �������� ����");
            LOG.info("�������� ������� �� ����");
        } catch (Throwable e) {
            UtilsInfo.warnLog(LOG, e);
        }
        return new TargetFinishPage(driver);
    }


}