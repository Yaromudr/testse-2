package testSe.utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import testSe.abstractEssence.SearchPage;
import testSe.search.GooglePage;
import testSe.search.RamblerPage;
import testSe.search.SearchNameConstants;
import testSe.search.YandexPage;

/**
 * Created by A.Dolinskiy on 06.08.2015.
 * ������� ��� ������ ������� ��� ������ ��������� ������� (�����, ������, �������)
 */
public class SearchFactory {

    private static final Logger LOG = Logger.getLogger(SearchFactory.class);

    private SearchFactory() {
    }

    /**
     * ��������� �����
     * @param hostname - ��������� ��������� �������
     * @param driver - ������� ��� �������
     * @return - �������� ��������� �������
     */
    public static SearchPage getPage(SearchNameConstants hostname, WebDriver driver) {

        //��������� �� ���������, ���� ��������� ��������� � main
        if (hostname.equals(SearchNameConstants.SEARCH_YA)) {
            return new YandexPage(driver);
        } else if (hostname.equals(SearchNameConstants.SEARCH_GO)) {
            return new GooglePage(driver);
        } else if (hostname.equals(SearchNameConstants.SEARCH_RA)) {
            return new RamblerPage(driver);
        } else
            return null;//�� ���� ������ �� ������
    }
}
