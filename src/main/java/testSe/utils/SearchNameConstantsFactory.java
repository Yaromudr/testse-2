package testSe.utils;

import org.apache.log4j.Logger;
import testSe.search.SearchNameConstants;

/**
 * ������� ��������� ������������� ��������� ������� � ���������� ������� Enum
 * Created by A.Dolinskiy on 12.08.2015.
 */
public class SearchNameConstantsFactory {
    private static final Logger LOG = Logger.getLogger(SearchNameConstantsFactory.class);

    /**
     * ��������� �����
     * @param arg - ������������� ��������� �������
     * @return - ��������� ��������� ������� �� ���������� ��� ������
     */
    public static SearchNameConstants getConstants(String arg) {

        try {
            assert ((arg.equals("yandex")) || (arg.equals("google")) || (arg.equals("rambler"))) : "��������� ������� ����������";
            LOG.info("��������� ������� ������� ���������. ��������: "+ arg);
        } catch (Throwable e) {
            UtilsInfo.warnLog(LOG,e);
        }

        switch (arg) {
            case "yandex":
                return SearchNameConstants.SEARCH_YA;
            case "google":
                return SearchNameConstants.SEARCH_GO;
            case "rambler":
                return SearchNameConstants.SEARCH_RA;
            default:
                return null;//�� ���� ������ ������ ������ �� ������. ������ ������ �� �������
        }
    }
}
