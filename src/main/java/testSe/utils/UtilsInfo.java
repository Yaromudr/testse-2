package testSe.utils;

import org.apache.log4j.Logger;
import testSe.start.Main;

/**
 * �������������� �����
 * Created by A.Dolinskiy on 13.08.2015.
 */
public class UtilsInfo {
    private static String TEXT;

    /**
     * ����� ��� ��������� ������
     * ���������� ����������� ������, ���������� ������ �������� � ������� (��� �����) ���� ��������� ������
     * @param LOG - ����� �������� ������
     * @param e - ������������� ������
     */
    public static void warnLog(Logger LOG, Throwable e) {
        LOG.warn("������: " + e.getMessage());
        if (Main.getDriver() != null) {
            try {
                Main.getDriver().quit();
                assert (Main.getDriver() == null) : "������� ���������� �� �������";
                LOG.info("������� ������� ����������");
            } catch (Throwable e1) {
                LOG.warn("Exit with error: " + e1.getMessage());
                System.exit(-666); //��������� �������� ����� ����� �� ��������� ����� ������?
            }
        }

        if (Main.WebDriwerRemotePROCESS != null) {
            try {
                Main.WebDriwerRemotePROCESS.destroy();
                assert (Main.WebDriwerRemotePROCESS == null) : "��������� ������ ���������� �� �������";
                LOG.info("��������� ������ ������� ����������");
            } catch (Throwable e1) {
                LOG.warn("Exit with error: " + e1.getMessage());
                System.exit(-666); //��������� �������� ����� ����� �� ��������� ����� ������?
            }
        }

        LOG.error("Exit with error: " + e.getMessage());
        System.exit(666); //��������� �������� ����� ����� �� ��������� ����� ������?
    }

    /**
     * �������� � �������� ��������� � ���������� ��� ���������.
     * ����� ������� �� ����� ��������
     */
    public static void printInfo() {
        TEXT = Main.getProp().getProperty("TEXT");
        System.out.println(TEXT);

    }
}
