package testSe.utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import testSe.start.Main;

import java.io.File;
import java.net.URL;

import static org.openqa.selenium.remote.DesiredCapabilities.chrome;

/**
 * ����� ��������� �������, ������������ WebDriver
 * Created by A.Dolinskiy on 11.08.2015.
 */
public class WebDriverFactory {

    private static final Logger LOG = Logger.getLogger(WebDriverFactory.class);

    private static String IEWebDriverPath = Main.getProp().getProperty("IEWebDriverPath");
    private static String ChromeWebDriverPath = Main.getProp().getProperty("ChromeWebDriverPath");

    /**
     * ��������� �����
     * @param arg - ������������� ���������� ��������
     * @return - WebDriver � ������ ��������������.
     */
    public static WebDriver getDriver(String arg) {
        try {
            assert (!arg.isEmpty()):"������ ������� �������� �� ���������";
            switch (arg) {
                case "ie":
                    LOG.info("������ �������� ���������. ��������: " + arg);
                    DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
                    capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
                    assert (!IEWebDriverPath.isEmpty()):"����� IE ������� �� ������";
                    LOG.info("����� ������� ������");
                    assert ((new File(IEWebDriverPath)).exists()):"���� �� ������ ������� �� ����������";
                    System.setProperty("webdriver.ie.driver", IEWebDriverPath);
                    LOG.info("������� �������: IE Driver");
                    return new InternetExplorerDriver(capabilities);
                case "ff":
                    LOG.info("������ �������� ���������. ��������: "+arg);
                    LOG.info("������� �������: FF Driver");
                    return new FirefoxDriver();
                case "gch":
                    LOG.info("������ �������� ���������. ��������: "+arg);
                    assert (!ChromeWebDriverPath.isEmpty()):"����� ���� ������� �� ������";
                    LOG.info("����� ������� ������");
                    assert ((new File(ChromeWebDriverPath)).canExecute()):"���� �� ������ ������� �� �����������";
                    Main.WebDriwerRemotePROCESS = Runtime.getRuntime().exec(ChromeWebDriverPath);
                    assert(Main.WebDriwerRemotePROCESS!=null):"������ ��� ����� �� ����������";
                    LOG.info("ChromeDriverServer �������");
                    URL url = new URL("http://localhost:9515");
                    LOG.info("������� �������: Chrome Driver");
                    return new RemoteWebDriver(url, chrome());
                default:
                    LOG.warn("������ �������� �� ���������! ��������: "+arg);
                    LOG.warn("������� ������ �� �������");
                    UtilsInfo.printInfo();
                    return null;
            }
        } catch (Throwable e) {
            UtilsInfo.warnLog(LOG, e);
            return null;//��������, � ���������� ������ ����� �����
        }
    }
}
